﻿/*
 * BodyView.cs
 *
 * Displays spheres for Kinect body joints
 * Requires the BodyDataConverter script or the BodyDataReceiver script
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using System;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;

public class BodyView : MonoBehaviour {

    public int speed = 10,hp=30;
    public Rigidbody Bullet;
    public GameObject BodySourceManager;
    private bool LeftShooted = false,RightShooted = false;
    // Dictionary relating tracking IDs to displayed GameObjects
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodyDataConverter _BodyDataConverter;
    private BodyDataReceiver _BodyDataReceiver;
    public Vector3 startPosition;
    public TextMesh LeftTxt,RightTxt,EnemyLife;
    private void Start()
    {
        //startPosition = Camera.main.transform.position;
    }

    private void UpdateText() {
        if (Camera.main != null)
        {
            if (LeftTxt != null)
            {
                
                    //LeftTxt.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1.0f + Camera.main.transform.up * 0.05f - Camera.main.transform.right * 0.2f;
                    //LeftTxt.transform.localRotation = Camera.main.transform.localRotation;
            }
            if (RightTxt != null)
            {
                
                    //RightTxt.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1.0f + Camera.main.transform.up * 0.05f + Camera.main.transform.right * 0.1f;
                    //RightTxt.transform.localRotation = Camera.main.transform.localRotation;
            }
        }
    }
    void Update() {
        UpdateText();
        if (BodySourceManager == null) {
            return;
        }

        // Dictionary of tracked bodies from the Kinect or from data
        // sent over the server
        Dictionary<ulong, Vector3[]> bodies;

        // Is the body data coming from the BodyDataConverter script?
        _BodyDataConverter = BodySourceManager.GetComponent<BodyDataConverter>();
        if (_BodyDataConverter == null) {
            // Is the body data coming from the BodyDataReceriver script?
            _BodyDataReceiver = BodySourceManager.GetComponent<BodyDataReceiver>();
            if (_BodyDataReceiver == null) {
                return;
            } else {
                bodies = _BodyDataReceiver.GetData();
            }
        } else {
            bodies = _BodyDataConverter.GetData();
        }

        if (bodies == null) {
            return;
        }

        // Delete untracked bodies
        List<ulong> trackedIDs = new List<ulong>(bodies.Keys);
        List<ulong> knownIDs = new List<ulong>(_Bodies.Keys);
        foreach (ulong trackingID in knownIDs) {
             //Debug.Log("trackingID "+trackingID);
            if (!trackedIDs.Contains(trackingID)) {
                Destroy(_Bodies[trackingID]);
                _Bodies.Remove(trackingID);
            }
        }

        // Add and update tracked bodies
        foreach (ulong trackingID in bodies.Keys) {

            // Add tracked bodies if they are not already being displayed
            if (!_Bodies.ContainsKey(trackingID)) {
                _Bodies[trackingID] = CreateBodyObject(trackingID);
            }

            // Update the positions of each body's joints
            RefreshBodyObject(bodies[trackingID], _Bodies[trackingID]);
        }
    }

    // Create a GameObject given a tracking ID
    private GameObject CreateBodyObject(ulong id) {
        GameObject body = new GameObject("Body:" + id);
        body.transform.parent = Camera.main.transform;
        body.transform.position = startPosition;
        body.transform.localRotation = Camera.main.transform.localRotation;
        body.transform.localScale = new Vector3(0.2f, 0.2f, 1f);
        for (int i = 0; i < 25; i++) {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            jointObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            jointObj.name = i.ToString();
            jointObj.transform.parent = body.transform;
        }
        return body;
    }

    // Update the joint GameObjects of a given body
    private void RefreshBodyObject(Vector3[] jointPositions, GameObject bodyObj) {
        for (int i = 0; i < 25; i++) {
            Vector3 jointPos = jointPositions[i];
            jointPos.x = jointPos.x * 10.0f;
            jointPos.y = jointPos.y * 10.0f;
            jointPos.z = jointPos.z * 2;
            Transform jointObj = bodyObj.transform.Find(i.ToString());
            jointObj.localPosition = jointPos;
        }
    }
}
